import React from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';

const ImageDetail = ({imageSource, title, imageScore}) => {
    return(
        <View>
            <Image source={imageSource}/>
            <Text>{title}</Text>
            <Text>Image Score - {imageScore}</Text>
        </View>
    );
}

const style = StyleSheet.create({

});

export default ImageDetail;