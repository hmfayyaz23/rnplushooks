import React, {useState} from 'react';
import {Text, View, Button, StyleSheet} from 'react-native';

const CounterScreen = () => {
    const [counter, setCounter] = useState(0);
    function decreaseCounter(){
        setCounter(counter - 1);
    }
    return(
        <View>
            <Button 
                title="Increase"
                onPress={() => {
                    setCounter(counter + 1)
                }}
            />

            <Button 
                title="Decrease"
                onPress={decreaseCounter}
            />

            <Text>Current Count: {counter}</Text>
        </View>
    );
}

const style = StyleSheet.create({

});

export default CounterScreen;