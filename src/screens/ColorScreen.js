import React, {useState} from 'react';
import {View, Button, FlatList, StyleSheet} from 'react-native';

const ColorScreen = () => {
    const [colors, setColors] = useState([]);
    handleChangeColor = () => {
        setColors([ ...colors, randomRGB()])
    }
    renderColors = (item) => {
        return(
            <View style={{ height:100, width: 100, backgroundColor: item }}/>
        );
    }
    return(
        <View>
            <Button title="Add a color" onPress={handleChangeColor} />
            <FlatList 
                data={colors}
                keyExtractor={item => item}
                renderItem={({ item }) => renderColors(item)}
            />
        </View>
    );
}

const randomRGB = () => {
    let red = Math.floor( Math.random() * 256);
    let green = Math.floor( Math.random() * 256);
    let blue = Math.floor( Math.random() * 256);

    return `rgb(${red}, ${green}, ${blue})`;
}

const styles = StyleSheet.create({

});

export default ColorScreen;